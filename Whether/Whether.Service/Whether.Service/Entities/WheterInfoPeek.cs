﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Whether.Service.Entities
{
    [DataContract]
    public class WheterInfoPeek
    {
        [DataMember]
        public DateTime Create { get; set; }

        [DataMember]
        public long Temperature { get; set; }

        [DataMember]
        public long Light { get; set; }
    }
}
