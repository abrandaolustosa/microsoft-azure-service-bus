﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Whether.Service.Entities
{
    [DataContract]
    public class WhetherInfoResponse
    {
        [DataMember]
        public bool Success { get; set; }
    }
}
