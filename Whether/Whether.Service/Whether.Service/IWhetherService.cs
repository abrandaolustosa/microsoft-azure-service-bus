﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;

namespace Whether.Service
{
    [ServiceContract]
    public interface IWhetherService
    {
        [OperationContract]
        Entities.WhetherInfoResponse Create(Entities.WheterInfoRequest wheterInfoRequest);

        [OperationContract]
        Entities.WheterInfoPeek Peek();
    }
}
