﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Whether.Service.Proces;

namespace Whether.Service.Manager
{
    public class WheterManager
    {
        private WhetherProcess whetherProcess = null;
        public WheterManager()
        {
            this.whetherProcess = new WhetherProcess();
        }
        public Entities.WhetherInfoResponse Create(Entities.WheterInfoRequest wheterInfoRequest)
        {
            Entities.WhetherInfoResponse whetherInfoResponse = new Entities.WhetherInfoResponse()
            {
                Success = false,
            };

            Entities.WheterInfo wheterInfo = Mapper.MapperLibrary.WheterInfoRequestToWheterInfo(wheterInfoRequest);

            whetherInfoResponse.Success = this.whetherProcess.Create(wheterInfo);

            return whetherInfoResponse;
        }

        public Entities.WheterInfoPeek Peek()
        {
            Entities.WheterInfoPeek wheterInfoPeek = null;

            Entities.WheterInfo wheterInfo = this.whetherProcess.Peek();

            if(wheterInfo != null)
            {
                wheterInfoPeek = new Entities.WheterInfoPeek(){                     
                    Create = wheterInfo.Create,
                    Temperature = wheterInfo.Temperature,
                    Light = wheterInfo.Light,
                };
            }

            return wheterInfoPeek;
        }
    }
}
