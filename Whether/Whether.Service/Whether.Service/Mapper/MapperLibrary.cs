﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whether.Service.Mapper
{
    public class MapperLibrary
    {
        public static Entities.WheterInfo WheterInfoRequestToWheterInfo(Entities.WheterInfoRequest wheterInfoRequest)
        {
            Entities.WheterInfo wheterInfo = new Entities.WheterInfo()
            {
                Create = DateTime.UtcNow,
                Light = wheterInfoRequest.Light,
                Temperature = wheterInfoRequest.Temperature,
            };

            return wheterInfo;
        }
    }
}
