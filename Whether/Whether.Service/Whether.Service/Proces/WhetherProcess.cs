﻿using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Whether.Service.Proces
{
    public class WhetherProcess
    {
        private QueueClient Client;            

        public WhetherProcess()
        {
            string busConnectionString = CloudConfigurationManager.GetSetting("InsideBusService");

            this.Client = QueueClient.CreateFromConnectionString(busConnectionString, "whether");
        }
        public bool Create(Entities.WheterInfo wheterInfo)
        {
            bool result = false;
            
            BrokeredMessage message = new BrokeredMessage(wheterInfo);
            
            Client.Send(message);

            result = true;

            return result;
        }

        public Entities.WheterInfo Peek()
        {
            Entities.WheterInfo wheterInfo = null;

            BrokeredMessage message = Client.Peek();

            if(message != null)
            {
                wheterInfo = message.GetBody<Entities.WheterInfo>();
            }

            return wheterInfo;
        }
    }
}

