﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.ServiceModel.Activation;
using System.ServiceModel.Web;
using System.Text;
using Whether.Service.Entities;

namespace Whether.Service
{
    [AspNetCompatibilityRequirements(RequirementsMode = AspNetCompatibilityRequirementsMode.Allowed)]
    public class WhetherService : IWhetherService
    {
        private static Manager.WheterManager wheterManager = null;

        private static object syncObj = new object();
        public WhetherService()
        {
            if(WhetherService.wheterManager == null)
            {
                lock (syncObj)
                {
                    if (WhetherService.wheterManager == null)
                    {
                        WhetherService.wheterManager = new Manager.WheterManager();
                    }
                }
            }
        }
        public WhetherInfoResponse Create(WheterInfoRequest wheterInfoRequest)
        {
            return WhetherService.wheterManager.Create(wheterInfoRequest);
        }

        public WheterInfoPeek Peek()
        {
            return WhetherService.wheterManager.Peek();
        }
    }
}
